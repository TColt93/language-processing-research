import lexer
import par as parser
import fetcher
import json
from datetime import datetime

def main(types):
    
    content = ''
    #Read the current text in test.lang and store it in variable All
    content = ''
    with open('test.lang', 'r') as file:
        All = file.read()
        file.close()

    All = All.split(';')

    # Initialize the counter
    c = 0

    # Loop through All and find the id and the text
    for item in All:
        if c == 0:
            identity = item
        if c == 1:
            content = item
        c =+ 1

    #Read the past log files in test.lang and store it in variable log
    with open('log.txt', 'r') as file:
        log = file.read()
        file.close()
    #
    # Fetcher
    #

    # We call the fetcher class and intialize it with the past data
    fetch = fetcher.Fetcher(identity)
    previous_attempt = fetch.fetch_conversation(log)

    #
    # Lexer
    #

    # We call the lexer class and initialize it with the Text
    lex = lexer.Lexer(content)
    # We now call the tokenize method
    tokens = lex.tonkenize(types)

    #
    # Parser
    #

    # Initialize the output arrays to store the items for the log

    parse = parser.Parser(previous_attempt, tokens)
    self = parse.parse()

    output_create_and_log(self)

    if self.confirmation == "True":
        response = []
        if self.action_type == 'pay':
            return_val = False
            response.append('CALL PAYMENT API WITH PARAMETERS - return RESPONSE')
            return_val = True
            if return_val == True:
                self.valid_parameters = "None"
                self.previous_timestamp = "None"
                self.state = 0
                self.action_type = "None"
                self.confirmation = "None"
                response.append('THE PAYMENT HAS BEEN MADE SUCCESSFULLY')
            elif return_val == False:
                response.append('FAIL - RESPONSE from API call return')
        self.response = response

        output_create_and_log(self)

def output_create_and_log(self):
    jsonoutput = []
    outputarray = []
    outputarray.append(self.ident)
    outputarray.append(self.action_type)
    outputarray.append(self.valid_parameters)
    outputarray.append(self.state)
    if self.previous_timestamp != "None":
        outputarray.append(str(datetime.now()))
    else:
        outputarray.append("None")
    outputarray.append(self.confirmation)

    jsonoutput = json.dumps(outputarray)

    with open('log.txt', 'a') as file:
        file.write(jsonoutput + '\n')

    for line in self.response:
        print(line)

def fetch_all():
    outputs = []
    folder = 'Types/'
    files = ['actions', 'conjunctions', 'parameters', 'prepositions', 'pronouns', 'nouns', 'accept', 'reject', 'adjectives']

    for f in files:
        wordarray = []
        with open(folder + '{}.txt'.format(f), 'r') as file:
            words = file.read()

        wordarray = words.split('\n')

        outputs.append(wordarray)

    return outputs

types = fetch_all()
main(types)