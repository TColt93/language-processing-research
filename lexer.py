import re

class Lexer(object):

    def __init__(self, text):
        self.text = text

    def tonkenize(self, types):
        
        # Where all the tokens created by lexer will be stored
        tokens = []

        text = self.text

        # Create a word list of the text
        text = text.split()

        # This will keep track of the word index we are at in the text
        index = 0

        # Loop through each word in text to generate tokens
        while index < len(text):

            word = text[index]

            # This will recognise a ACTION and create a token for it
            if word in types[0]: tokens.append(['ACTION', word])

            # This will recognise a CONJUNCTION and create a token for it
            elif word in types[1]: tokens.append(['CONJUNCTION', word])

            # This will recognise an PARAMETER and create a token for it
            elif word in types[2]: tokens.append(['PARAMETER', word])

            # This will recognise an PREPOSITION and create a token for it
            elif word in types[3]: tokens.append(['PREPOSITION', word])

            # This will recognise an PRONOUN and create a token for it
            elif word in types[4]: tokens.append(['PRONOUN', word])

            # This will recognise an NOUN and create a token for it
            elif word in types[5]: tokens.append(['NOUN', word])

            # This will recognise an NOUN and create a token for it
            elif word in types[6]: tokens.append(['ACCEPT', word])

            # This will recognise an NOUN and create a token for it
            elif word in types[7]: tokens.append(['REJECTION', word])

            # This will recognise an NOUN and create a token for it
            elif word in types[8]: tokens.append(['ADJECTIVE', word])

            # This will recognise an AMOUNT and create a token for it
            elif re.match('[0-9]', word[1]) and re.match('R', word[0]): tokens.append(['AMOUNT', word])

            # This will recognise an ACCOUNT and create a token for it
            elif re.match('[0-9]', word) and len(word) >= 9 : tokens.append(['ACCOUNT_NUM', word])

            # This will recognise an INTEGER and create a token for it
            elif re.match('[0-9]', word) and len(word) < 9 : tokens.append(['INTEGER', word])

            # This will recognise an operator and create a token for it
            elif word in '=/*=-+': tokens.append(['OPERATOR', word])

            # This will recognise anything that is left and create a token for it
            else: tokens.append(['THING', word])

            index += 1

        print(tokens)

        return tokens
