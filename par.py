class Parser(object):
    def __init__(self, previous_attempt, tokens):
        # This will hold all the tokens that have been created by the lexer
        self.tokens = tokens
        # This will hold the token index we are parsing at
        self.token_index = 0
        self.previous_timestamp = previous_attempt[0]
        self.ident = previous_attempt[1]
        self.action_type = previous_attempt[2]
        self.past_parameters = previous_attempt[3]
        self.state = previous_attempt[4]
        self.confirmation = previous_attempt[5]

    def parse(self):
        while self.token_index < len(self.tokens):
            
            # Holds the type of tokens
            token_type = self.tokens[self.token_index][0]

            # Holds the value of tokens
            token_value = self.tokens[self.token_index][1]

            if token_type == 'ACTION' and token_value == 'hello':
                if len(self.tokens) > 1:
                    self.token_index += 1
                    continue
                else:
                    self.hello_function()

            if token_type == 'ACTION' and token_value == 'help':
                if len(self.tokens) > 2:
                    self.token_index += 1
                    continue
                elif len(self.tokens) == 2:
                    self.help_function()
                else:
                    self.help_function()

            # This will trigger when a variable declaration token is found
            if self.state == 0:
                if token_type == 'ACTION' and token_value == 'pay':
                    self.action_type = 'pay'
                    self.check_payment(self.tokens[self.token_index:len(self.tokens)])
                    break

            elif self.state == 1:
                if self.action_type == 'pay':
                    self.check_payment(self.tokens)
                    break

            elif self.state == 2 or self.state == 3:
                if self.action_type == 'pay':
                    self.confirm(self.tokens)
                    break

            

            # Increment token index by 1 so we can loop through next token
            self.token_index += 1

        return self

    def check_payment(self, token_stream):

        # Initialize the necessary variables to hold the counter and array of parameters
        completed = 0
        parameters_found = []

        # Create a full list of parameters to create a valid payment request
        full_parameters = [['account_holder'],['account_number'],['branch_code'],['bank_name'],['transfer_value'],['my_reference'],['their_reference'],['reference']]

        if self.past_parameters != 'None':
            for param in self.past_parameters:
                cnt = 0
                for parameter in full_parameters:
                    p = param[0]
                    if parameter[0] == p:
                        full_parameters[cnt].append(param[1])
                        break

                    cnt += 1
        # Loop through the token_stream
        for token in range(0, len(token_stream)):

            # create variables for token type and value 
            token_type = token_stream[completed][0]
            token_value = token_stream[completed][1]

            # Look for all the tokens with type PARAMETER
            if token_type == 'PARAMETER':
                # Add these parameters to a new array holding each instance of [the count value and parameters found] 
                parameters_found.append([completed, token_value])

            # Increase the counter
            completed += 1
        
        # Initialize the counter variable
        completed = 0

        # Loop through the parameters found
        for param in parameters_found:
            
            param[1] = param[1].replace(':', '')

            # Initialize the variables needed for the loop function
            parameter_values = []
            parameter_value = ''

            # find the position of where the parameter was found
            position = param[0]

            # Find the length of the entire parameter found array
            length = len(parameters_found) - 1

            # IF the current position is less than the length of the array
            if completed < length:
                # create the next parameter variable (helps when searching for parameter values)
                position_next = parameters_found[completed+1][0]
            else:
                # create the next parameter variable (helps when searching for parameter values)
                # use the end length when its the last paramter found 
                position_next = len(token_stream)
                    
            # Loop through the range between the current parameter and the next parameter
            for val in range(position, position_next):

                # Fetch the type of word currently being looked at
                ttype = token_stream[val][0]
                # Fetch the value of the word we are currently looking at
                tval = token_stream[val][1]

                # IF the type fits the following criteria
                if ttype == 'THING' or ttype == 'AMOUNT' or ttype == 'ACCOUNT_NUM' or ttype == 'INTEGER':
                    # Add to the values found between the current and next parameter in the token stream
                    parameter_values.append(tval)

            # Find the length of the values array
            lenVal = len(parameter_values)
            # Initialize the counter
            cnt = 0

            # Loop through the values found to create a value string which we add to the parameter found array
            for val in parameter_values:
                if cnt < lenVal - 1:
                    parameter_value = parameter_value + val + ' '
                else:
                    parameter_value = parameter_value + val
                
                # Increase counter
                cnt += 1
            
            # Initialize the counter
            cnt = 0

            # Loop through the full parameter array to find which parameter we are adding a value to
            for parameter in full_parameters:
                p = param[1]
                if parameter[0] == p:
                    if len(parameter) == 1:
                        full_parameters[cnt].append(parameter_value)
                    if len(parameter) == 2:
                        del full_parameters[cnt][1]
                        full_parameters[cnt].append(parameter_value)
                    break
                
                # Increase counter
                cnt += 1 

            # Increase the counter
            completed += 1

        # Initialize the valid and invalid parameter and reference arrays
        valid_parameters = []
        invalid_parameters = []
        valid_references = []
        invalid_references = []

        # loop through the full parameter array and validate which ones are missing
        for param in full_parameters[:5]:
            # if the length of the param is more than 1 value
            if len(param) == 1:
                invalid_parameters.append(param[0])
            else:
                valid_parameters.append(param)

        for param in full_parameters[5:len(full_parameters)]:
            # if the length of the param is more than 1 value
            if len(param) == 1:
                invalid_references.append(param[0])
            else:
                valid_references.append(param)

        if (len(valid_references) == 2) and (valid_references[0][0] == 'my_reference') and (valid_references[1][0] == 'their_reference'):
            valid_parameters.append(valid_references[0])
            valid_parameters.append(valid_references[1])
            invalid_references = []
        
        elif (len(valid_references) == 1) and (valid_references[0] == 'reference'):
            valid_parameters.append(['my_reference',valid_references[1]])
            valid_parameters.append(['their_reference',valid_references[1]])
            invalid_references = []

        elif (len(valid_references) == 1):
            valid_parameters.append(valid_references[0])
        
        elif (len(valid_references) == 3):
            valid_parameters.append(valid_references[0])
            valid_parameters.append(valid_references[1])
            invalid_references = []

        self.valid_parameters = valid_parameters
        if self.state < 2:
            state, response = pay_response_function(self, valid_parameters, invalid_parameters, valid_references, invalid_references)

            self.response = response
            self.state = state

    def confirm(self, tokens):
        # Initialize variables and counters needed
        response = []

        if len(tokens) > 1 or (tokens[0][0] != 'ACCEPT' and tokens[0][0] != 'REJECTION'):
            response.append('Please answer with either:\n')
            response.append('   yes - to confirm \n')
            response.append('        OR \n')
            response.append('   no - to change details \n')
            response.append('THANK YOU :)')

            self.valid_parameters = self.past_parameters
            self.response = response

        elif tokens[0][1] == 'yes':
            if self.state == 2:
                response.append('Your payment is currently being validated. \n')
                response.append('We will get back to you shortly with Proof of Payment\n')
                response.append('THANK YOU :)')

                self.valid_parameters = self.past_parameters
                self.response = response
                self.state = 4
                self.confirmation = "True"

            elif self.state == 3:
                response.append('To ammend a payment details type the following for each change: \n')
                response.append('<detail-name>: <detail-value> \n')
                response.append('CLICK SEND to make all your changes')

                self.valid_parameters = self.past_parameters
                self.response = response
                self.state = 1

        elif tokens[0][1] == 'no':
            if self.state == 2:
                response.append('Would you like to ammend any of the following details: \n')

                for param in self.past_parameters:
                    response.append(param[0] + ': ' + param[1])

                response.append('\n (yes/no) ')

                self.valid_parameters = self.past_parameters
                self.response = response
                self.state = 3

            elif self.state == 3:
                response.append('Thank You for using Whatsapp Banking')
                response.append('If you would like to use any of the other features:\n')
                response.append('type "pay" to make a payment' )

                self.response = response
                self.ident = "None"
                self.action_type = "None"
                self.valid_parameters = "None"
                self.state = 0
                self.previous_timestamp = "None"
                self.Confirmation = "None"

    def hello_function(self):
        response = []
        response.append('Please choose below from the list of actions available:\n\n')
        response.append('pay - make a payment to someone\n')
        response.append('help: <action> - to receive a list of necessary parameters for that action\n')
        response.append('\n')
        response.append('reply with one of the above action requests')

    def help_function(self):
        response = []
        response.append('Which action would you like help with:\n\n')
        response.append('pay - make a payment to someone\n')
        response.append('help: <action> - to receive a list of necessary parameters for that action\n')
        response.append('\n')
        response.append('reply with one of the above action requests')

def pay_response_function(self, valid_parameters, invalid_parameters, valid_references, invalid_references):

    # Initialize the printer array
    response = []

    # IF there are missing parameters
    if len(valid_parameters) < 7:
        if len(valid_parameters) > 0:
            # Print the following message  in the below format back to the person messaging
            response.append('The Following details have been captured \n')

            # Print the valid parameters and their values
            for valid in valid_parameters:
                response.append(valid[0] + ': ' + valid[1])
            
        response.append('The Following details need to be captured \n')

        # Print the invalid parameters 
        for invalid in invalid_parameters:
            response.append(invalid + ': <' + invalid + '>')

        if len(invalid_references) > 0:
            if len(invalid_references) == 1 and invalid_references[0] == 'my_reference':
                # append missing reference needed
                response.append('and\n  their_reference: <their-reference>')

            elif len(invalid_references) == 1 and invalid_references[0] == 'their_reference':
                # append missing reference needed
                response.append('and\n  my_reference: <your-reference>')

            else:
                response.append('and EITHER:\n  my_reference: <your-reference> \n  their_reference: <their-reference> \n')

                response.append('OR:\n  reference: <reference-for-both-parties>')

        response.append('\n'+'Please respond with the above missing parameters to complete the payment')

        state = 1

    # IF all the parameters have been entered
    else:
        # Print the following message  in the below format back to the person messaging
        response.append('Please confirm the following details before you accept this payment \n')

        # Print the valid parameters and their values
        for valid in valid_parameters:
            response.append(valid[0] + ': ' + valid[1])

        response.append('\n' + 'Do you wish to continue with this payment? (yes/no)')

        state = 2
    
    return state, response
    