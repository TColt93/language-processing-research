from ast import literal_eval as strToList
from datetime import datetime

class Fetcher(object):

    def __init__(self, identity):
        # This will hold the identity of the user we are answering
        self.identity = identity

    def fetch_conversation(self, log):
        # Initialize the found variable to track whether previous records were found
        found = False

        identity = self.identity

        # Create an array of logs and items
        logs = log.split('\n')
        log_items = []

        for item in logs:
            if item != '':
                log_items.append(item)

        # Loop through the log items
        for item in reversed(log_items):
            # Split the item details to find the elemnts
            details = strToList(item)
            ident = details[0]
            action_type = details[1]
            past_parameters = details[2]
            state = details[3]
            previous_timestamp = datetime.strptime(details[4], '%Y-%m-%d %H:%M:%S.%f')
            Confirmation = details[5]

            if ident == identity:
                found = True
                break
            else:
                continue

        if found == False:
            ident = "None"
            action_type = "None"
            past_parameters = "None"
            state = 0
            previous_timestamp = "None"
            Confirmation = "None"

        return (previous_timestamp,ident,action_type,past_parameters,state, Confirmation)
        